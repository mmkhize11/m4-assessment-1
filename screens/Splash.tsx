import React from 'react'
import { StyleSheet, Text, View,StatusBar,Image } from 'react-native'
import {Colorstobeused} from '../constants/color'




const Splash = ({navigation}) => {

    setTimeout(()=>{
        navigation.replace('Login')
    },3000)




    return (
        <View style={{flex:1,flexDirection:'column',justifyContent:'center',alignItems:'center',backgroundColor:'#f93b50'}} >
            <StatusBar barStyle="light-content" hidden={false} backgroundColor="#f93b50" />
            {/* <Image source={require('../assets/splash.png')}   /> */}
            <Image source={require('../assets/icon.png')} style={{width:350,height:350}}  />    
            {/* <Text style={{fontFamily:'OpenSans-Bold',fontSize:30,color:Colorstobeused.white}} >Mkhize Chique</Text> */}
        </View>
    )
}

export default Splash

const styles = StyleSheet.create({})