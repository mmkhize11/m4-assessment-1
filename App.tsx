import React, { useState,useEffect  } from 'react';
import {View,Text} from 'react-native'
import { NavigationContainer } from '@react-navigation/native'
// import { createNativeStackNavigator } from '@react-native-masked-view/masked-view';
// import {createAppContainer} from "react-navigation" 
import {Splash,Login} from './screens'
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import * as Font from 'expo-font';
// import { AppLoading } from 'expo';
import AppLoading from 'expo-app-loading';
import Home from './screens/Home';
import {createRoot} from 'react-dom/client';
// import App from './App';
import {BrowserRouter as Router} from 'react-router-dom';
import Signup from './screens/Signup';
import ProductDetail from './screens/ProductDetail';
import DashboardScreen from './screens/Dashboard';
// import ax from './assets/fonts/';
import { NativeBaseProvider } from 'native-base';
import Scanner from './screens/ScanItemPage';
import DiscountList from './screens/DiscountsList';
const Stack = createNativeStackNavigator();




const App = ()=>{




useEffect(() => {
  Font.loadAsync({
    'OpenSans-Bold': require('./assets/fonts/OpenSans-Bold.ttf'),
    'OpenSans-BoldItalic': require('./assets/fonts/OpenSans-BoldItalic.ttf'),
    'OpenSans-ExtraBold': require('./assets/fonts/OpenSans-ExtraBold.ttf'),
    'OpenSans-ExtraBoldItalic': require('./assets/fonts/OpenSans-ExtraBoldItalic.ttf'),
    'OpenSans-Italic': require('./assets/fonts/OpenSans-Italic.ttf'),
    'OpenSans-Light': require('./assets/fonts/OpenSans-Light.ttf'),
    'OpenSans-LightItalic': require('./assets/fonts/OpenSans-LightItalic.ttf'),
    'OpenSans-Medium': require('./assets/fonts/OpenSans-Medium.ttf'),
    'OpenSans-MediumItalic': require('./assets/fonts/OpenSans-MediumItalic.ttf'),
    'OpenSans-Regular': require('./assets/fonts/OpenSans-Regular.ttf'),
    'OpenSans-SemiBold': require('./assets/fonts/OpenSans-SemiBold.ttf'),
    'OpenSans-SemiBoldItalic': require('./assets/fonts/OpenSans-SemiBoldItalic.ttf')
  })
  },[])


  return (
    <NativeBaseProvider>
          <NavigationContainer>
            <Stack.Navigator screenOptions={{headerShown:false}} >
              {/* add possible screen routes here  */}
              <Stack.Screen name="Splash" component={Splash} />

           

              <Stack.Screen name="Login"  >
              {(props) => <Login {...props}   />}
                </Stack.Screen>
              <Stack.Screen name="Signup" component={Signup} />

              <Stack.Screen name="Home"  >
              {(props) => <Home {...props} />}
                </Stack.Screen>
              

                <Stack.Screen name="ProductDetailNew"  >
              {(props) => <ProductDetail {...props}   />}
                </Stack.Screen>

                <Stack.Screen name="DashboardScreen"  >
              {(props) => <DashboardScreen {...props}    />}
                </Stack.Screen>

                <Stack.Screen name="Scanner"  >
              {(props) => <Scanner  {...props} />}
                </Stack.Screen>


                <Stack.Screen name="DiscountList"  >
              {(props) => <DiscountList  user={undefined} {...props} />}
                </Stack.Screen>

                {/* DiscountList */}

{/* Scanner */}

                {/* DashboardScreen */}
              {/* <Stack.Screen name="Home" component={Home} /> */}
              
            </Stack.Navigator>
          </NavigationContainer>
          </NativeBaseProvider>
  );
}

export default App